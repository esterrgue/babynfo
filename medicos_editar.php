<?php
// Busca la información de la consulta que se quiere editar
// y crea el formulario para su edición
session_start();

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

$id_cita = $_GET["id"];
$id_hijo = $_SESSION["id_hijo"];

// Cuando enviamos el formulario con la información que queremos actualizar:
if(isset($_POST['enviar'])) { 

    $fecha_nueva = $_POST["fecha"] . " " . $_POST["hora"];
    $lugar_nuevo = $_POST["lugar"];
    $id_cita_nueva = $_POST["tarea"];
    $descripcion_nueva = $_POST["descripcion"];
    $tratamiento_nuevo = $_POST["tratamiento"];
    $id_consulta_nueva = $_POST["tipo-consulta"];
    $id_cita = $_POST["id_cita"];

    $actualizar_cita_sql = <<<SQL

        UPDATE visita_medico 
           SET lugar_cita = '{$lugar_nuevo}',
               fecha_cita = '{$fecha_nueva}',
               descripcion_cita = '{$descripcion_nueva}',
               tratamiento_cita = '{$tratamiento_nuevo}',
               id_consulta = {$id_consulta_nueva}
        WHERE id_cita = {$id_cita}

SQL; 

    $actualizar_cita = mysqli_query($conexion, $actualizar_cita_sql);
    if (!$actualizar_cita) {
        echo "Error SQL actualizar_cita: " . $actualizar_cita_sql;
        exit();
    } else {
        header("Location: medicos.php?id=" . $id_hijo);
    }

} else {

    $buscar_tipos_consulta_sql = <<<SQL

        SELECT id_consulta,
               nombre_consulta
        FROM consulta
        ORDER BY 2

SQL;

    $buscar_tipos_consulta = mysqli_query($conexion, $buscar_tipos_consulta_sql);
    if (!$buscar_tipos_consulta) {
        echo "Error SQL buscar_tipos_consulta: ";
        echo $buscar_tipos_consulta_sql;
    }

    while ($resultado = mysqli_fetch_array($buscar_tipos_consulta)) {
        $tipos_consulta[] = [
            "id"        => $resultado["id_consulta"],
            "nombre"    => $resultado["nombre_consulta"]
        ];
    }

    $buscar_consultas_sql = <<<SQL

        SELECT v.id_cita,
               v.lugar_cita,
               v.fecha_cita,
               c.nombre_consulta,
               c.id_consulta,
               v.descripcion_cita,
               v.tratamiento_cita
        FROM visita_medico v
        INNER JOIN consulta c
           ON v.id_consulta = c.id_consulta
        WHERE v.id_cita = {$id_cita}

SQL;

    $buscar_consultas = mysqli_query($conexion, $buscar_consultas_sql);
    if (!$buscar_consultas) {
        echo "Error SQL buscar_consultas" . $buscar_consultas_sql;
    } else {
        while ($resultado = mysqli_fetch_array($buscar_consultas)) {
            $cita = [
                "id"            => $resultado["id_cita"],
                "fecha"         => $resultado["fecha_cita"],
                "lugar"         => $resultado["lugar_cita"],
                "id_consulta"   => $resultado["id_consulta"],
                "consulta"      => $resultado["nombre_consulta"],
                "tratamiento"   => $resultado["tratamiento_cita"],
                "descripcion"   => $resultado["descripcion_cita"]
            ];
        }
    }

    $fecha_cita = (new DateTime($cita["fecha"]))->format("Y-m-d");
    $fecha_hora_cita = (new DateTime($cita["fecha"]))->format("H:i");
    $lugar_cita = $cita["lugar"];
    $tratamiento_cita = $cita["tratamiento"];
    $descripcion_cita = $cita["descripcion"];   
    $id_consulta = $cita["id_consulta"];

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <!-- Comienza el body -->
    <body>
    <!-- Cabecera: Panel de navegación -->
<?php 
    // Añadimos el menú
    require_once("navegacion.php");
?>
        <div class="container shadow">
            <div class="row central pb-5">
                <div class="col">
                    <h1>Editar consulta</h1>
                </div>
                <div class="w-100"></div>
                <div class="col">
                    <form method="post" action="medicos_editar.php">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label for="fecha">Fecha</label>
                                <input type="date" name="fecha" class="form-control" value="<?php echo $fecha_cita; ?>" required>
                            </div>
                            <div class="col-auto">
                                <label for="hora">Hora</label>
                                <input type="time" name="hora" class="form-control" value="<?php echo $fecha_hora_cita; ?>" required>
                            </div>
                            <div class="col-auto">
                                <label for="lugar">Lugar</label>
                                <input type="text" name="lugar" class="form-control" value="<?php echo $lugar_cita; ?>" required>
                            </div>
                            <div class="col-auto">
                                <label for="tarea">Tipo de consulta</label>
                                <select name="tipo-consulta" class="custom-select">
                                    <option value="" disabled required selected>Elige una consulta</option>
            <?php 
            foreach ($tipos_consulta as $consulta) {
            	if ($consulta["id"] == $id_consulta) {
            		echo "
                                    <option value=\"" . $consulta["id"] . "\" selected>" . $consulta["nombre"] . "</option>";
            	} else {
            		echo "
                                    <option value=\"" . $consulta["id"] . "\">" . $consulta["nombre"] . "</option>";
                }
            }
            ?>                        
                                </select>
                            </div>
                            <div class="col-auto">
                                <label for="descripcion">Descripción</label>
                                <textarea name="descripcion" required class="form-control"><?php echo $descripcion_cita; ?></textarea>
                            </div>
                            <div class="col-auto">
                                <label for="tratamiento">Tratamiento</label>
                                <textarea name="tratamiento" required class="form-control"><?php echo $tratamiento_cita; ?></textarea>
                            </div>
                            <div class="col-auto">
                                <input hidden type="text" name="id_cita" value="<?php echo $id_cita; ?>">
                                <input type="submit" name="enviar" value="Enviar" class="form-control">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php require_once("footer.php"); ?>
    </body>
</html>
<?php 
}
?>