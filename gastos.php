<?php
session_start();

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

$id_hijo = $_GET["id"];
$_SESSION["id_hijo"] = $id_hijo;

if(isset($_POST['enviar'])){ 

    $fecha = $_POST["fecha"] . " " . $_POST["hora"];
    $lugar = $_POST["lugar"];
    $id_pagador = $_POST["pagador"];
    $id_gasto = $_POST["tipo-gasto"];
    $importe = $_POST["importe"];
    $descripcion = $_POST["descripcion"];

    $insertar_gasto_sql = <<<SQL

        INSERT INTO pago (
            descripcion_pago,
            fecha_pago,
            importe_pago,
            id_gasto,
            id_hijo,
            id_pagador
        ) VALUE (
            '{$descripcion}',
            '{$fecha}',
            {$importe},
            {$id_gasto},
            {$id_hijo},
            {$id_pagador}
        )

SQL;

    $insertar_gasto = mysqli_query($conexion, $insertar_gasto_sql);
    if (!$insertar_gasto) {
        echo "Error SQL insertar_gasto: " . $insertar_gasto_sql;
    } else {
        header("Location: gastos.php?id=" . $id_hijo);
    }
} 

if (isset($_SESSION["usuario"])) {

    $buscar_tipos_gasto_sql = <<<SQL

        SELECT id_gasto,
               nombre_gasto
        FROM tipo_gasto
        ORDER BY 2

SQL;

    $buscar_tipos_gasto = mysqli_query($conexion, $buscar_tipos_gasto_sql);
    if (!$buscar_tipos_gasto) {
        echo "Error SQL buscar_tipos_gasto: ";
        echo $buscar_tipos_gasto_sql;
    }

    while ($resultado = mysqli_fetch_array($buscar_tipos_gasto)) {
        $tipos_gasto[] = [
            "id"        => $resultado["id_gasto"],
            "nombre"    => $resultado["nombre_gasto"]
        ];
    }

    // Buscamos los posibles pagadores
    $buscar_pagadores_sql = <<<SQL

        SELECT id_pagador,
               nombre_pagador
        FROM pagadores
        ORDER BY 2

SQL;

    $buscar_pagadores = mysqli_query($conexion, $buscar_pagadores_sql);
    if (!$buscar_pagadores) {
        echo "Error SQL buscar_pagadores: ";
        echo $buscar_pagadores_sql;
    }

    while ($resultado = mysqli_fetch_array($buscar_pagadores)) {
        $pagadores[] = [
            "id"        => $resultado["id_pagador"],
            "nombre"    => $resultado["nombre_pagador"]
        ];
    }

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Panel de navegación -->
<?php 
    // Añadimos el menú
    require_once("navegacion.php");

    // Array en el que almacenaremos los gastos
    $gastos = [];

    $buscar_gastos_sql = <<<SQL

        SELECT p.id_pago,
               p.fecha_pago,
               p.importe_pago,
               p.descripcion_pago,
               p.id_pagador,
               pa.nombre_pagador,
               tp.nombre_gasto
        FROM pago p
        INNER JOIN pagadores pa
           ON p.id_pagador = pa.id_pagador
        INNER JOIN tipo_gasto tp
           ON p.id_gasto = tp.id_gasto
        WHERE p.id_hijo = {$id_hijo}

SQL;

    $buscar_gastos = mysqli_query($conexion, $buscar_gastos_sql);
    if (!$buscar_gastos) {
        echo "Error SQL buscar_gastos" . $buscar_gastos_sql;
    } else {
        while ($resultado = mysqli_fetch_array($buscar_gastos)) {
            $gastos[] = [
                "id"            => $resultado["id_pago"],
                "fecha"         => $resultado["fecha_pago"],
                "importe"       => $resultado["importe_pago"],
                "pagador"       => $resultado["nombre_pagador"],
                "tipo_gasto"    => $resultado["nombre_gasto"],
                "descripcion"   => $resultado["descripcion_pago"]
            ];
        }
    }
?>
    <div class="container shadow">
      <div class="row central pb-5">
        <div class="col">
            <h1>Gastos</h1>
        </div>
        <div class="w-100"></div>
        <div class="col">
        <form method="post" action="">
            <div class="form-row align-items-center">
                <div class="col-auto">
                    <label for="fecha">Fecha</label>
                    <input type="date" name="fecha" class="form-control" required>
                </div>
                <div class="col-auto">
                    <label for="hora">Hora</label>
                    <input type="time" name="hora" class="form-control" required>
                </div>
                <div class="col-auto">
                    <label for="pagador">Pagador</label>
                    <select name="pagador" class="custom-select">
                        <option value="" disabled required selected>Elige un pagador</option>
<?php 
foreach ($pagadores as $pagador) {
    echo "
                        <option value=\"" . $pagador["id"] . "\">" . $pagador["nombre"] . "</option>";
}
?>                        
                    </select>
                </div>
                <div class="col-auto">
                    <label for="tarea">Tipo de gasto</label>
                    <select name="tipo-gasto" class="custom-select">
                        <option value="" disabled required selected>Elige una categoría</option>
<?php 
foreach ($tipos_gasto as $gasto) {
    echo "
                        <option value=\"" . $gasto["id"] . "\">" . $gasto["nombre"] . "</option>";
}
?>                        
                    </select>
                </div>
                <div class="col-auto mt-4">
                    <label for="importe">Importe</label>
                    <input type="number" step="0.01" name="importe" class="form-control" required>
                </div>
                <div class="col-auto mt-4">
                    <label for="descripcion">Descripción</label>
                    <textarea name="descripcion" required class="form-control"></textarea>
                </div>
                <div class="col-auto">
                    <input type="submit" name="enviar" value="Enviar" class="form-control">
                </div>
            </div>
        </form>
      </div>
  </div>
<?php 
if (count($gastos) != 0) {
?>
      <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table ">
                    <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>Pagador</th>
                      <th>Tipo gasto</th>
                      <th>Importe</th>
                      <th>Descripción</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
<?php 
    foreach ($gastos as $gasto) {
        $fecha_gasto = (new DateTime($gasto["fecha"]))->format("d/m/Y");
        $fecha_hora_gasto = (new DateTime($gasto["fecha"]))->format("H:i");
        $id_gasto = $gasto["id"];
        echo "
                        <tr id=\"gasto-{$id_gasto}\">
                            <td>{$fecha_gasto} {$fecha_hora_gasto}</td>
                            <td>{$gasto["pagador"]}</td>
                            <td>{$gasto["tipo_gasto"]}</td>
                            <td>" . number_format($gasto["importe"], 2, ",", ".") . " €</td>
                            <td>{$gasto["descripcion"]}</td>
                            <td><a href=\"gastos_editar.php?id={$id_gasto}\" class=\"boton\"><i class=\"fa fa-edit\"></i></a> 
                            <a href=\"gastos_borrar.php?id={$id_gasto}\"class=\"boton\"><i class=\"fa fa-trash\"></i></a>
                            </td>
                        </tr>";
    }
?>
                    </tbody>
                </table>
            </div>
        </div>
<?php
}
?>       
  </div>
  <?php require_once("footer.php"); ?>
  </body>
</html>
<?php 
} else {
  header("Location: index.php");
}
?>