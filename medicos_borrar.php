<?php
// Elimina la consulta/cita pasada por URL

session_start();

$id_hijo = $_SESSION["id_hijo"];

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

if (isset($_GET["id"])) {

    $id_cita = $_GET["id"];

    $borrar_cita_sql = <<<SQL

        DELETE 
        FROM visita_medico
        WHERE id_cita = {$id_cita}

SQL;

    $borrar_cita = mysqli_query($conexion, $borrar_cita_sql);
    if (!$borrar_cita) {
        echo "Error SQL borrar_cita: " . $borrar_cita_sql;
    } else {
        header("Location: medicos.php?id=" . $id_hijo);
    }
}

