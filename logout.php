<?php
session_start();

session_destroy();
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Panel de navegación, Titulo e Imagen -->
    <nav class="navbar navbar-light bg-light">
      <a href="index.php" class="navbar-brand btn btn-light">Iniciar sesión</a>
    </nav>
    <div class="container shadow">
      <div class="row central">
        <div class="col-sm col-md col-xl">
          <header>
            <h1>Sesión cerrada. Hasta pronto.</h1>
            <img class="img-fluid rounded border border-dark" src="images/img12.jpg">
          </header>
        </div>
      </div>
    </div>
  </body>
</html>