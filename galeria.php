<?php
session_start();

if (isset($_SESSION["usuario"])) {

    require_once("conexion-bd.php");
    $id_hijo = $_GET["id"];

    // Array que almacenará las imágenes del bebé.
    $imagenes = [];
    
    if (isset($_POST["subir-imagen"])) {
        //
        // Subimos la imagen
        // ================================================
        $ruta_usuario = __DIR__ . "/users/" . $_SESSION["alias"];
        // Si no existe el directorio donde almacena el usuario 
        // las imágenes, lo creamos
        if (!is_dir($ruta_usuario)) {
            mkdir($ruta_usuario);
        }

        // Definimos la ruta donde se guardará la imagen (directorio_usuario/nombre_imagen.jpg)
        $ruta_imagen = $_SESSION["alias"] . "/" . round(microtime(true)) . "_" . $_FILES["imagen"]["name"];

        // Definimos la ruta absoluta del servidor donde se guardará la imagen
        // (/var/www/html/users/directorio_usuario/nombre_imagen.jpg)
        $ruta_absoluta_imagen = __DIR__ . "/users/" . $ruta_imagen;
        if (!move_uploaded_file($_FILES['imagen']['tmp_name'], $ruta_absoluta_imagen)) {
            //echo "ERROR subiendo imagen: #" . $_FILES["imagen"]["error"];
            $error_subida = true;
        } else {
            //echo "Imagen subida correctamente";
            $error_subida = false;

            // Insertamos en la base de datos la información de la imagen
            $insertar_imagen_sql = <<<SQL

                INSERT INTO imagenes (
                    ruta_foto,
                    id_hijo
                ) VALUES (
                    '{$ruta_imagen}',
                    {$id_hijo}
                )
SQL;

            $insertar_imagen = mysqli_query($conexion, $insertar_imagen_sql);
            if (!$insertar_imagen) {
                echo "Error SQL: insertar_imagen: ";
                echo $insertar_imagen_sql;
            }
        }
    } // fin subida imágenes
    
    $buscar_imagenes_sql = <<<SQL

        SELECT ruta_foto
        FROM imagenes
        WHERE id_hijo = {$id_hijo}

SQL;

    $buscar_imagenes = mysqli_query($conexion, $buscar_imagenes_sql);

    while ($imagen = mysqli_fetch_array($buscar_imagenes)) {
        $imagenes[] = $imagen["ruta_foto"];
    }
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- Galería -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/css/lightgallery.min.css" integrity="sha256-8rfHbJr+ju3Oc099jFJMR1xAPu8CTPHU8uP5J3X/VAY=" crossorigin="anonymous">
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
<?php
    // Añadimos el menú
    require_once("navegacion.php");
?>
    <!-- Cabecera: Titulo e Imagen -->
        <div class="container shadow">
            <div class="row central pb-5">
                <div class="col">
                    <h1>Galería de fotos</h1>
                </div>
                <div class="w-100"></div>
                <div class="col-lg-8 offset-lg-2">
                    <form method="post" action="#" enctype="multipart/form-data">
                        <label>Subir imagen</label>
                        <input type="file" name="imagen">
                        <input type="submit" name="subir-imagen" value="Enviar">
                    </form>
                </div>
                <div class="w-100"></div>
                <div class="col">
<?php
    if (count($imagenes) != 0) {

?>
                    <div id="lightgallery">
<?php
       
        // Galería
        // --------------------------------------------------------------------
        foreach ($imagenes as $imagen) {
             echo "<a href=\"users/{$imagen}\"><img src=\"users/{$imagen}\" class=\"img-galeria\"></a>";
        } 
?>

                    </div>
<?php 
    }
?>
                </div>
            </div>
        </div>
    <?php require_once("footer.php"); ?>
    <!-- 
        Galería JavaScript: 
            https://sachinchoolur.github.io/lightGallery/ 
    -->
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js" integrity="sha256-GnThhVDusd6Mnhplk3lS6eX/C+Q9jaSR6KctYP8OlAo=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/modules/lg-thumbnail.min.js" integrity="sha256-Ck/PWQUoHsc9d2X4yUALvrs71Qo5cs+gIHDWBl2Ggb8=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/modules/lg-zoom.min.js" integrity="sha256-8HKk4FQYhQmgSPX+eWa4NrDtKnmYv1FzubCwxi97jdA=" crossorigin="anonymous"></script>
    <script type="text/javascript">

        // Llamada y configuración del plugin de la galería
        $(document).ready(function() {
            $("#lightgallery").lightGallery({
                mode: "lg-fade"
            }); 
        });
    </script>
  </body>
</html>
<?php 
} else {
  header("Location: index.php");
} 
?>