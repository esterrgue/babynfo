<?php
session_start();

if (!isset($_SESSION["usuario"])) {
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Titulo e Imagen -->
    <div class="container">
      <header class="index">
        <h1>Babynfo</h1>
        <img class="img-fluid rounded mx-auto d-block" src="images/img10.jpg">
      </header> 
    </div>
    <!-- Contenido: 1 FILA con 2 COLUMNAS -->
    <div class="container shadow">
      <div class="row">
      	<!-- Contenido: Información -->
        <div class="col-sm col-md col-xl informacion">
          <p>Babynfo es una aplicación web creada para mantener ordenada toda la información sobre tus retoños. Tiene secciones específicas para obtener y guardar de manera sencilla los registros del médico, los horarios del bebé, sus fotos...</p>
        </div>
        <!-- Contenido: Formulario -->
        <div class="col-sm col-md col-xl informacion">
          <form action="login.php" method="post">
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="periquito@babynfo.com" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Contraseña</label>
              <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="********" required>
            </div>
            <input type="submit" class="btn btn-primary" value="Enviar">
          </form>
        </div>
      </div>
      <!-- Contenido: Regístrate -->
      <div class="row">
        <div class="col-sm col-md col-xl informacion">
          <p>¿No tienes cuenta en Babynfo? <a class="btn btn-primary" href="registro.php" role="button">Regístrate</a></p>
        </div>
      </div>
    </div>
  </body>
</html>
<?php 
} else {
	header("Location: principal.php");
}
?>