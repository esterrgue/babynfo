<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- 2 columas: Imagen y Título-->
    <div class="container shadow">
      <div class="row central">
        <div class="col-sm col-md col-xl">
          <header>
            <h1>Regístrate</h1>
            <img class="img-fluid rounded border border-dark" src="images/img13.jpg">
          </header>
        </div>
        <div class="col-sm col-md col-xl informacionform">
          <!-- En la etiqueta "form" empleamos los atributos:
            * method: para indicar el método que usaremos para enviar los datos del formulario
            * action: para indicar la página que recibirá los datos del formulario
          -->
          <form method="post" action="registrado.php">
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre</label>
              <input type="text" class="form-control" name="nombre" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Apellido</label>
              <input type="text" class="form-control" name="apellido" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Alias</label>
              <input type="text" class="form-control" name="alias" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Email</label>
              <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Contraseña</label>
              <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="" required>
            </div>
            <input type="submit" class="btn btn-primary" value="Enviar">
          </form>
        </div>   
      </div>
      <div class="row">
        <div class="col">
            <p class="text-center"><a href="./">Volver a la página principal</a></p>
        </div>
      </div>
    </div>
  </body>
</html>