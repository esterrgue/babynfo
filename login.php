<?php
// Comprueba si las credenciales introducidas son
// correctas para poder iniciar sesión el usuario
session_start();
require_once("conexion-bd.php");

$clave_introducida = $_POST["password"];
$email_introducido = $_POST["email"];

$comprobar_usuario_bd_sql = <<<SQL

	SELECT id_usu,
	       alias,
	       password
	FROM usuario
	WHERE email = '{$email_introducido}'

SQL;

$comprobar_usuario_bd = mysqli_query($conexion, $comprobar_usuario_bd_sql);
if (!$comprobar_usuario_bd) {
	echo "Error SQL: comprobar_usuario_bd: ";
	echo $comprobar_usuario_bd_sql;
}
$datos_bd = mysqli_fetch_array($comprobar_usuario_bd);
$id_usuario_bd = $datos_bd[0];
$alias_bd = $datos_bd[1];
$clave_bd = $datos_bd[2];

if ($clave_bd === $clave_introducida) {
	// echo "Bienvenido!";
	// Si el usuario ha introducido los datos correctos,
	// creamos variables de sesión para tenerlo identificado
	// en el resto de páginas
	$_SESSION["usuario"] = $email_introducido;
	$_SESSION["alias"] = $alias_bd;
	$_SESSION["id"] = $id_usuario_bd;
	header("Location: principal.php");
} else {
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Titulo e Imagen -->
    <div class="container">
      <header class="index">
        <h1>Babynfo</h1>
        <img class="img-fluid rounded mx-auto d-block" src="images/img10.jpg">
      </header> 
    </div>
    <!-- Contenido: 1 FILA con 2 COLUMNAS -->
    <div class="container">
      <div class="row">
      	<!-- Contenido: Información -->
<?php
	echo "
				<div class=\"col-lg-6 offset-lg-3\">";
	echo "
					<div class=\"alert alert-danger\" role=\"alert\">
						Usuario o contraseña no válida. <a href=\"./\">Vuelve a intentarlo.</a>
					</div>
				</div>";
}

?>
			</div> <!-- row -->
		</div> <!-- container -->
	</body>
</html>