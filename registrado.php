<?php
/*
	Página que recibe los datos del formulario de registro,
	los procesa y los mete en la base de datos para finalizar
	el registro del usuario
*/
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Titulo e Imagen -->
		<div class="container">
			<div class="row mt-3">
				<div class="col-lg-8 offset-lg-2">
<?php
require_once("conexion-bd.php");

$buscar_usuario_sql = <<<SQL

	SELECT *
	FROM usuario
	WHERE alias = '{$_POST["alias"]}' 

SQL;

$buscar_usuario = mysqli_query($conexion, $buscar_usuario_sql);
$numero_usuarios = mysqli_num_rows($buscar_usuario);

if ($numero_usuarios != 0) {
	echo "
				<div class=\"alert alert-danger text-center\">
					Error: ya existe un usuario con ese alias. <a href=\"registro.php\">Vuelve a intentarlo</a>.
				</div>";
} else {
	// Cuando no existe un usuario con ese alias en la base de datos,
	// lo añadiremos.
	$insertar_usuario_sql = <<<SQL

		INSERT INTO usuario (
			nombre,
			apellido,
			alias,
			email,
			password
		) VALUES (
			'{$_POST["nombre"]}',
			'{$_POST["apellido"]}',
			'{$_POST["alias"]}',
			'{$_POST["email"]}',
			'{$_POST["password"]}'
		)

SQL;

	$insertar_usuario = mysqli_query($conexion, $insertar_usuario_sql);

	echo "<p class=\"text-center\">¡Usuario creado! Ya puedes <a href=\"principal.php\">iniciar sesión</a></p>";
}
?>
				</div>
			</div>
		</div>
	</body>
</html>