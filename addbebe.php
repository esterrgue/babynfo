<?php
session_start();

if (isset($_SESSION["usuario"])) {
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Panel de navegación, Titulo e Imagen -->
    <nav class="navbar navbar-light bg-light">
      <a href="principal.php" class="navbar-brand btn btn-light">Home</a>
      <a href="logout.php" class="navbar-brand btn btn-light">Cerrar sesión</a>
    </nav>
    <!-- Formulario añadir un bebe -->
    <div class="container shadow">
      <div class="row central">
        <div class="col-sm col-md col-xl">
          <header>
            <h1>Añadir bebé</h1>
            <img class="img-fluid rounded" src="images/img10.jpg">
          </header>
        </div>
        <div class="col-sm col-md col-xl addbb">
          <form method="post" action="addedbebe.php" enctype="multipart/form-data">
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre</label>
              <input type="text" class="form-control" name="nombre" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Apellidos</label>
              <input type="text" class="form-control" name="apellidos" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Sexo</label>
                <select name="sexo" class="custom-select" required>
                  <option selected disabled>Escoge</option>
                  <option value="a">Niña</option>
                  <option value="o">Niño</option>
                </select>
              </div>
               <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Fecha Nacimiento</label>
                <input type="date" step="1" name="fecha" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
              </div>
              <div class="form-group col-md-4">
                <label for="exampleInputEmail1">Hora</label>
                <input type="time" step="1" name="hora" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="" required>
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="exampleInputPassword1">Peso al nacer</label>
                <input type="number" name="peso" step="0.001" min="1" max="7" class="form-control" id="exampleInputPassword1" placeholder="" required>
              </div>
              <div class="form-group col-md-4">
                <label for="exampleInputPassword1">Altura al nacer</label>
                <input type="number" name="altura" step="0.01" class="form-control" id="exampleInputPassword1" placeholder="" required>
              </div>
              <div class="form-group col-md-4">
                <label for="exampleInputPassword1">Grupo sanguíneo</label>
                <input type="text" name="grupo-sangre" class="form-control" id="exampleInputPassword1" placeholder="" required> 
              </div>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Descripción</label>
              <textarea class="form-control" name="descripcion" id="exampleInputPassword1" placeholder="" required></textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Imagen</label>
              <input type="file" name="imagen" class="form-control" id="exampleInputPassword1" placeholder="" required>
            </div>
            <input type="submit" class="btn btn-primary" value="Enviar">
          </form>
        </div>   
      </div>
    </div>
  </body>
</html>
<?php 
} else {
  header("Location: index.php");
}
?>