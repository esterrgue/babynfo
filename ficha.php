<?php
session_start();

if (isset($_SESSION["usuario"])) {

    // Incluimos los datos de conexión con la base de datos
    require_once("conexion-bd.php");

    $id_bebe = $_GET["id"];
    $_SESSION["id_bebe"] = $id_bebe;

    $buscar_bebe_sql = <<<SQL

        SELECT nombre,
               apellidos,
               sexo,
               fecha_nac,
               peso_nacer,
               altura_nacer,
               grupo_sanguineo,
               descripcion,
               imagen_hijo
        FROM hijo
        WHERE id_hijo = {$id_bebe}

SQL;

    $buscar_bebe = mysqli_query($conexion, $buscar_bebe_sql);
    if (!$buscar_bebe) {
        echo "Error SQL buscar_bebe: ";
        echo $buscar_bebe_sql;
    }

    while ($resultado = mysqli_fetch_array($buscar_bebe)) {
        $informacion_bebe = [
            "nombre"    => $resultado["nombre"],
            "apellidos"    => $resultado["apellidos"],
            "sexo"    => $resultado["sexo"],
            "fecha_nacimiento"    => $resultado["fecha_nac"],
            "peso_nacer"    => $resultado["peso_nacer"],
            "altura_nacer"    => $resultado["altura_nacer"],
            "grupo_sanguineo"    => $resultado["grupo_sanguineo"],
            "descripcion"    => $resultado["descripcion"],
            "imagen_hijo"    => $resultado["imagen_hijo"]
        ];
    }

    $fecha_nacimiento = (new DateTime($informacion_bebe["fecha_nacimiento"]))->format("d/m/Y");
    $hora_nacimiento = (new DateTime($informacion_bebe["fecha_nacimiento"]))->format("H:i");
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Panel de navegación -->
<?php 
    // Añadimos el menú
    require_once("navegacion.php");
?>
    <div class="container shadow">
      <div class="row central">
        <div class="col-sm col-md col-xl">
          <img src="<?php echo $informacion_bebe["imagen_hijo"]; ?>" alt="..." class="img-thumbnail border fichaimg">
        </div>
        <div class="col-sm col-md col-xl">
          <h1>Ficha</h1>
          <p>Nombre: <?php echo $informacion_bebe["nombre"]; ?></p>
          <p>Apellidos: <?php echo $informacion_bebe["apellidos"]; ?></p>
          <p>Sexo: <?php echo $informacion_bebe["sexo"]; ?></p>
          <p>Fecha de Nacimiento: <?php echo $fecha_nacimiento . " " . $hora_nacimiento; ?></p>
          <p>Peso: <?php echo number_format($informacion_bebe["peso_nacer"], 3, ",", "."); ?> kg</p>
          <p>Altura: <?php echo number_format($informacion_bebe["altura_nacer"], 1, ",", "."); ?> cm</p>
          <p>Grupo sanguíneo: <?php echo $informacion_bebe["grupo_sanguineo"]; ?></p>
          <p>Descripción: </p>
          <p><?php echo $informacion_bebe["descripcion"]; ?></p>
        </div>
      </div>
    </div>
    <?php require_once("footer.php"); ?>
  </body>
</html>
<?php 
} else {
  header("Location: index.php");
}
?>