<?php
// Elimina el horario pasado por URL

session_start();

$id_hijo = $_SESSION["id_hijo"];

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

if (isset($_GET["id"])) {

    $id_horario = $_GET["id"];

    $borrar_horario_sql = <<<SQL

        DELETE 
        FROM horario
        WHERE id_horario = {$id_horario}

SQL;

    $borrar_horario = mysqli_query($conexion, $borrar_horario_sql);
    if (!$borrar_horario) {
        echo "Error SQL borrar_horario: " . $borrar_horario_sql;
    } else {
        header("Location: horario.php?id=" . $id_hijo);
    }
}

