<?php
session_start();

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

$id_hijo = $_GET["id"];
$_SESSION["id_hijo"] = $id_hijo;

if(isset($_POST['enviar'])){ 

    $fecha = $_POST["fecha"] . " " . $_POST["hora"];
    $lugar = $_POST["lugar"];
    $id_tipo_consulta = $_POST["tipo-consulta"];
    $descripcion = $_POST["descripcion"];
    $tratamiento = $_POST["tratamiento"];

    $insertar_consulta_sql = <<<SQL

        INSERT INTO visita_medico (
            lugar_cita,
            fecha_cita,
            descripcion_cita,
            tratamiento_cita,
            id_consulta,
            id_hijo
        ) VALUE (
            '{$lugar}',
            '{$fecha}',
            '{$descripcion}',
            '{$tratamiento}',
            {$id_tipo_consulta},
            {$id_hijo}
        )

SQL;

    $insertar_consulta = mysqli_query($conexion, $insertar_consulta_sql);
    if (!$insertar_consulta) {
        echo "Error SQL insertar_consulta: " . $insertar_consulta_sql;
    } else {
        header("Location: medicos.php?id=" . $id_hijo);
    }
} 

if (isset($_SESSION["usuario"])) {

    $buscar_tipos_consulta_sql = <<<SQL

        SELECT id_consulta,
               nombre_consulta
        FROM consulta
        ORDER BY 2

SQL;

    $buscar_tipos_consulta = mysqli_query($conexion, $buscar_tipos_consulta_sql);
    if (!$buscar_tipos_consulta) {
        echo "Error SQL buscar_tipos_consulta: ";
        echo $buscar_tipos_consulta_sql;
    }

    while ($resultado = mysqli_fetch_array($buscar_tipos_consulta)) {
        $tipos_consulta[] = [
            "id"        => $resultado["id_consulta"],
            "nombre"    => $resultado["nombre_consulta"]
        ];
    }

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Panel de navegación -->
<?php 
    // Añadimos el menú
    require_once("navegacion.php");

    // Array en el que almacenaremos las citas
    $citas = [];

    $buscar_consultas_sql = <<<SQL

        SELECT v.id_cita,
               v.lugar_cita,
               v.fecha_cita,
               c.nombre_consulta,
               v.descripcion_cita,
               v.tratamiento_cita
        FROM visita_medico v
        INNER JOIN consulta c
           ON v.id_consulta = c.id_consulta
        WHERE v.id_hijo = {$id_hijo}

SQL;

    $buscar_consultas = mysqli_query($conexion, $buscar_consultas_sql);
    if (!$buscar_consultas) {
        echo "Error SQL buscar_consultas" . $buscar_consultas_sql;
    } else {
        while ($resultado = mysqli_fetch_array($buscar_consultas)) {
            $citas[] = [
                "id"            => $resultado["id_cita"],
                "fecha"         => $resultado["fecha_cita"],
                "lugar"         => $resultado["lugar_cita"],
                "consulta"      => $resultado["nombre_consulta"],
                "tratamiento"   => $resultado["tratamiento_cita"],
                "descripcion"   => $resultado["descripcion_cita"]
            ];
        }
    }
?>
    <div class="container shadow">
      <div class="row central pb-5">
        <div class="col">
            <h1>Médicos</h1>
        </div>
        <div class="w-100"></div>
        <div class="col">
        <form method="post" action="">
            <div class="form-row align-items-center">
                <div class="col-auto">
                    <label for="fecha">Fecha</label>
                    <input type="date" name="fecha" class="form-control" required>
                </div>
                <div class="col-auto">
                    <label for="hora">Hora</label>
                    <input type="time" name="hora" class="form-control" required>
                </div>
                <div class="col-auto">
                    <label for="hora">Lugar</label>
                    <input type="text" name="lugar" class="form-control" required>
                </div>
                <div class="col-auto">
                    <label for="tarea">Tipo de consulta</label>
                    <select name="tipo-consulta" class="custom-select">
                        <option value="" disabled required selected>Elige una consulta</option>
<?php 
foreach ($tipos_consulta as $consulta) {
    echo "
                        <option value=\"" . $consulta["id"] . "\">" . $consulta["nombre"] . "</option>";
}
?>                        
                    </select>
                </div>
                <div class="col-auto mt-4">
                    <label for="descripcion">Descripción</label>
                    <textarea name="descripcion" required class="form-control"></textarea>
                </div>
                <div class="col-auto mt-4">
                    <label for="tratamiento">Tratamiento</label>
                    <textarea name="tratamiento" required class="form-control"></textarea>
                </div>
                <div class="col-auto">
                    <input type="submit" name="enviar" value="Enviar" class="form-control">
                </div>
            </div>
        </form>
      </div>
  </div>
<?php 
if (count($citas) != 0) {
?>
      <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>Lugar</th>
                      <th>Consulta</th>
                      <th>Descripción</th>
                      <th>Tratamiento</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
<?php 
    foreach ($citas as $cita) {
        $fecha_cita = (new DateTime($cita["fecha"]))->format("d/m/Y");
        $fecha_hora_cita = (new DateTime($cita["fecha"]))->format("H:i");
        $id_cita = $cita["id"];
        echo "
                        <tr id=\"cita-{$id_cita}\">
                            <td>{$fecha_cita} {$fecha_hora_cita}</td>
                            <td>{$cita["lugar"]}</td>
                            <td>{$cita["consulta"]}</td>
                            <td>{$cita["descripcion"]}</td>
                            <td>{$cita["tratamiento"]}</td>
                            <td><a href=\"medicos_editar.php?id={$id_cita}\" class=\"boton\"><i class=\"fa fa-edit\"></i></a> 
                            <a href=\"medicos_borrar.php?id={$id_cita}\"class=\"boton\"><i class=\"fa fa-trash\"></i></a>
                            </td>
                        </tr>";
    }
?>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
<?php
}
?>       
  </div>
  <?php require_once("footer.php"); ?>
  </body>
</html>
<?php 
} else {
  header("Location: index.php");
}
?>