<?php
// Elimina el gasto pasado por URL

session_start();

$id_hijo = $_SESSION["id_hijo"];

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

if (isset($_GET["id"])) {

    $id_pago = $_GET["id"];

    $borrar_gasto_sql = <<<SQL

        DELETE 
        FROM pago
        WHERE id_pago = {$id_pago}

SQL;

    $borrar_gasto = mysqli_query($conexion, $borrar_gasto_sql);
    if (!$borrar_gasto) {
        echo "Error SQL borrar_gasto: " . $borrar_gasto_sql;
    } else {
        header("Location: gastos.php?id=" . $id_hijo);
    }
}

