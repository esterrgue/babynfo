<?php
session_start();

if (isset($_SESSION["usuario"])) {

    // Incluimos los datos de conexión con la base de datos
    require_once("conexion-bd.php");

    // Consultamos si el usuario conectado tiene algún hijo añadido
    $buscar_hijos_sql = <<<SQL

        SELECT t1.id_hijo,
               t2.nombre,
               t2.apellidos
        FROM hijos_usuarios t1
        INNER JOIN hijo t2
        ON t1.id_hijo = t2.id_hijo
        WHERE t1.id_usu = {$_SESSION["id"]}

SQL;

    
    $hijos = [];
    $buscar_hijos = mysqli_query($conexion, $buscar_hijos_sql);
    if (!$buscar_hijos) {
        echo "Error SQL buscar_hijos: ";
        echo $buscar_hijos_sql;
    }
    while ($resultado = mysqli_fetch_array($buscar_hijos)) {
        $hijos[] = [
            "id" => $resultado["id_hijo"],
            "nombre" => $resultado["nombre"],
            "apellidos" => $resultado["apellidos"]
        ];
    }

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Panel de navegación, Titulo e Imagen -->
    <nav class="navbar navbar-light bg-light">
      <a href="addbebe.php" class="navbar-brand btn btn-light">Añadir bebé</a>
      <a href="logout.php" class="navbar-brand btn btn-light">Cerrar sesión</a>
    </nav>
    <!-- Titulo y Lista de hijos a la izquierda -->
    <div class="container shadow">
      <div class="row central">
        <div class="col-sm col-md col-xl">
          <header>
            <h1>Babynfo</h1>
            <img class="img-fluid rounded border border-dark" src="images/img11.jpg">
          </header>
        </div>
        <div class="col-sm col-md col-xl">
          <h1>Mis bebés</h1>
          <!-- Lista de bebés -->
          <p>Estos son los bebés de <?php echo $_SESSION["alias"]; ?></p>
<?php 
if (count($hijos) > 0) {
    echo "
            <ul>";
    foreach ($hijos as $hijo) {
        echo "
                <li><a href=\"ficha.php?id=" . $hijo["id"] . "\">" . $hijo["nombre"] . " " . $hijo["apellidos"] . "</a></li>" . PHP_EOL;
    }
    echo "
            </ul>";
}
?>
        </div>    
      </div>
    </div>
  </body>
</html>
<?php 
} else {
    header("Location: index.php");
}

?>