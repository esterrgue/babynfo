<?php
// Busca la información del horario que se quiere editar
// y crea el formulario para su edición
session_start();

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

$id_horario = $_GET["id"];
$id_hijo = $_SESSION["id_hijo"];

// Cuando enviamos el formulario con la información que
// queremos actualizar:
if(isset($_POST['enviar'])){ 

    $fecha_nueva = $_POST["fecha"] . " " . $_POST["hora"];
    $id_tarea_nueva = $_POST["tarea"];
    $descripcion_nueva = $_POST["observaciones"];
    $id_horario = $_POST["id_horario"];

    $actualizar_horario_sql = <<<SQL

        UPDATE horario 
           SET fecha = '{$fecha_nueva}',
               id_tarea = {$id_tarea_nueva},
               descripcion = '{$descripcion_nueva}'
        WHERE id_horario = {$id_horario}

SQL; 

    $actualizar_horario = mysqli_query($conexion, $actualizar_horario_sql);
    if (!$actualizar_horario) {
        echo "Error SQL actualizar_horario: " . $actualizar_horario_sql;
        exit();
    } else {
        header("Location: horario.php?id=" . $id_hijo);
    }

} else {

    $buscar_tareas_sql = <<<SQL

            SELECT id_tarea,
                   nombre_tarea
            FROM tarea_horario
            ORDER BY 2

    SQL;

        $buscar_tareas = mysqli_query($conexion, $buscar_tareas_sql);
        if (!$buscar_tareas) {
            echo "Error SQL buscar_tareas_sql: ";
            echo $buscar_tareas_sql;
        }

        while ($resultado = mysqli_fetch_array($buscar_tareas)) {
            $tareas[] = [
                "id"    => $resultado["id_tarea"],
                "nombre"    => $resultado["nombre_tarea"]
            ];
        }

    $buscar_horario_sql = <<<SQL

            SELECT *
            FROM horario
            WHERE id_horario = {$id_horario}

    SQL;

        $buscar_horario = mysqli_query($conexion, $buscar_horario_sql);
        if (!$buscar_horario) {
            echo "Error SQL buscar_horario" . $buscar_horario_sql;
        } else {
            while ($resultado = mysqli_fetch_array($buscar_horario)) {
                $horario = [
                    "fecha"         => $resultado["fecha"],
                    "id_tarea"      => $resultado["id_tarea"],
                    "descripcion"   => $resultado["descripcion"]
                ];
            }
        }

        $fecha_horario = (new DateTime($horario["fecha"]))->format("Y-m-d");
        $fecha_hora_horario = (new DateTime($horario["fecha"]))->format("H:i");
        $descripcion = $horario["descripcion"];

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <!-- Comienza el body -->
    <body>
    <!-- Cabecera: Panel de navegación -->
<?php 
    // Añadimos el menú
    require_once("navegacion.php");
?>
        <div class="container shadow">
            <div class="row central pb-5">
                <div class="col">
                    <h1>Editar horario</h1>
                </div>
                <div class="w-100"></div>
                <div class="col">
                    <form method="post" action="horario_editar.php">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label for="fecha">Fecha</label>
                                <input type="date" name="fecha" class="form-control" value="<?php echo $fecha_horario; ?>" required>
                            </div>
                            <div class="col-auto">
                                <label for="hora">Hora</label>
                                <input type="time" name="hora" class="form-control" value="<?php echo $fecha_hora_horario; ?>" required>
                            </div>
                            <div class="col-auto">
                                <label for="tarea">Tarea</label>
                                <select name="tarea" class="custom-select">
                                    <option value="" disabled required>Elige una tarea</option>
            <?php 
            foreach ($tareas as $tarea) {
            	if ($horario["id_tarea"] == $tarea["id"]) {
            		echo "
                                    <option value=\"" . $tarea["id"] . "\" selected>" . $tarea["nombre"] . "</option>";
            	} else {
            		echo "
                                    <option value=\"" . $tarea["id"] . "\">" . $tarea["nombre"] . "</option>";
                }
            }
            ?>                        
                                </select>
                            </div>
                            <div class="col-auto">
                                <label for="observaciones">Observaciones</label>
                                <textarea name="observaciones" required class="form-control"><?php echo $descripcion; ?></textarea>
                            </div>
                            <div class="col-auto">
                                <input hidden type="text" name="id_horario" value="<?php echo $id_horario; ?>">
                                <input type="submit" name="enviar" value="Enviar" class="form-control">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php require_once("footer.php"); ?>
</body>
</html>
<?php 
}
?>