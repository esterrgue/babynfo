<?php
session_start();

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

$id_hijo = $_GET["id"];
$_SESSION["id_hijo"] = $id_hijo;

if(isset($_POST['enviar'])){ 

    $fecha = $_POST["fecha"] . " " . $_POST["hora"];
    $observaciones = $_POST["observaciones"];
    $id_tarea = $_POST["tarea"];

    $insertar_tarea_sql = <<<SQL

        INSERT INTO horario (
            fecha,
            descripcion,
            id_hijo,
            id_tarea
        ) VALUE (
            '{$fecha}',
            '{$observaciones}',
            {$id_hijo},
            {$id_tarea}
        )

SQL;

    $insertar_tarea = mysqli_query($conexion, $insertar_tarea_sql);
    if (!$insertar_tarea) {
        echo "Error SQL insertar_tarea: " . $insertar_tarea_sql;
    } else {
        header("Location: horario.php?id=" . $id_hijo);
    }
} 

if (isset($_SESSION["usuario"])) {

    $buscar_tareas_sql = <<<SQL

        SELECT id_tarea,
               nombre_tarea
        FROM tarea_horario
        ORDER BY 2

SQL;

    $buscar_tareas = mysqli_query($conexion, $buscar_tareas_sql);
    if (!$buscar_tareas) {
        echo "Error SQL buscar_tareas_sql: ";
        echo $buscar_tareas_sql;
    }

    while ($resultado = mysqli_fetch_array($buscar_tareas)) {
        $tareas[] = [
            "id"    => $resultado["id_tarea"],
            "nombre"    => $resultado["nombre_tarea"]
        ];
    }

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <!-- Comienza el body -->
  <body>
    <!-- Cabecera: Panel de navegación -->
<?php 
    // Añadimos el menú
    require_once("navegacion.php");

    // Array que almacenará todos los horarios del bebé
    $horario = [];

    $buscar_horario_sql = <<<SQL

        SELECT h.id_horario,
               h.fecha,
               t.nombre_tarea,
               h.descripcion
        FROM horario h
        INNER JOIN tarea_horario t
           ON h.id_tarea = t.id_tarea
        WHERE h.id_hijo = {$id_hijo}

SQL;

    $buscar_horario = mysqli_query($conexion, $buscar_horario_sql);
    if (!$buscar_horario) {
        echo "Error SQL buscar_horario" . $buscar_horario_sql;
    } else {
        while ($resultado = mysqli_fetch_array($buscar_horario)) {
            $horario[] = [
                "id"            => $resultado["id_horario"],
                "fecha"         => $resultado["fecha"],
                "tarea"         => $resultado["nombre_tarea"],
                "descripcion"   => $resultado["descripcion"]
            ];
        }
    }
?>
    <div class="container shadow">
      <div class="row central pb-5">
        <div class="col">
            <h1>Horario</h1>
        </div>
        <div class="w-100"></div>
        <div class="col">
            <form method="post" action="">
                <div class="form-row align-items-center">
                    <div class="col-auto">
                        <label for="fecha">Fecha</label>
                        <input type="date" name="fecha" class="form-control" required>
                    </div>
                    <div class="col-auto">
                        <label for="hora">Hora</label>
                        <input type="time" name="hora" class="form-control" required>
                    </div>
                    <div class="col-auto">
                        <label for="tarea">Tarea</label>
                        <select name="tarea" class="custom-select">
                            <option value="" disabled required selected>Elige una tarea</option>
<?php 
foreach ($tareas as $tarea) {
    echo "
                        <option value=\"" . $tarea["id"] . "\">" . $tarea["nombre"] . "</option>";
}
?>                        
                        </select>
                    </div>
                    <div class="col-auto">
                        <label for="observaciones">Observaciones</label>
                        <textarea name="observaciones" required class="form-control"></textarea>
                    </div>
                    <div class="col-auto">
                        <input type="submit" name="enviar" value="Enviar" class="form-control">
                    </div>
                </div>
            </form>
        </div>
      </div>
<?php 
if (count($horario) != 0) {
?>
      <div class="row">
        <div class="col">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                      <th>Fecha</th>
                      <th>Tarea</th>
                      <th>Observaciones</th>
                      <th></th>
                    </tr>
                    </thead>
                    <tbody>
<?php 
    foreach ($horario as $tareas_horario) {
        $fecha_horario = (new DateTime($tareas_horario["fecha"]))->format("d/m/Y");
        $fecha_hora_horario = (new DateTime($tareas_horario["fecha"]))->format("H:i");
        $id_horario = $tareas_horario["id"];
        echo "
                        <tr id=\"horario-{$id_horario}\">
                            <td>{$fecha_horario} {$fecha_hora_horario}</td>
                            <td>{$tareas_horario["tarea"]}</td>
                            <td>{$tareas_horario["descripcion"]}</td>
                            <td><a href=\"horario_editar.php?id={$id_horario}\" class=\"boton\"><i class=\"fa fa-edit\"></i></a> 
                            <a href=\"horario_borrar.php?id={$id_horario}\"class=\"boton\"><i class=\"fa fa-trash\"></i></a>
                            </td>
                        </tr>";
    }
?>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
<?php
}
?>       
  </div>
  <?php require_once("footer.php"); ?>
  </body>
</html>
<?php 
} else {
  header("Location: index.php");
}
?>