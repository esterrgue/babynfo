<?php
// Busca la información del gasto que se quiere editar
// y crea el formulario para su edición
session_start();

// Incluimos los datos de conexión con la base de datos
require_once("conexion-bd.php");

$id_gasto = $_GET["id"];
$id_hijo = $_SESSION["id_hijo"];

// Cuando enviamos el formulario con la información que queremos actualizar:
if(isset($_POST['enviar'])) { 

    $fecha_nueva = $_POST["fecha"] . " " . $_POST["hora"];
    $pagador_nuevo = $_POST["pagador"];
    $tipo_gasto_nuevo = $_POST["tipo-gasto"];
    $importe_gasto_nuevo = $_POST["importe"];
    $descripcion_nueva = $_POST["descripcion"];
    $id_gasto = $_POST["id_gasto"];

    $actualizar_pago_sql = <<<SQL

        UPDATE pago 
           SET descripcion_pago = '{$descripcion_nueva}',
               fecha_pago = '{$fecha_nueva}',
               importe_pago = {$importe_gasto_nuevo},
               id_gasto = {$tipo_gasto_nuevo},
               id_pagador = {$pagador_nuevo}
        WHERE id_pago = {$id_gasto}

SQL; 

    $actualizar_pago = mysqli_query($conexion, $actualizar_pago_sql);
    if (!$actualizar_pago) {
        echo "Error SQL actualizar_pago: " . $actualizar_pago_sql;
        exit();
    } else {
        header("Location: gastos.php?id=" . $id_hijo);
    }

} else {

    $buscar_tipos_gasto_sql = <<<SQL

        SELECT id_gasto,
               nombre_gasto
        FROM tipo_gasto
        ORDER BY 2

SQL;

    $buscar_tipos_gasto = mysqli_query($conexion, $buscar_tipos_gasto_sql);
    if (!$buscar_tipos_gasto) {
        echo "Error SQL buscar_tipos_gasto: ";
        echo $buscar_tipos_gasto_sql;
    }

    while ($resultado = mysqli_fetch_array($buscar_tipos_gasto)) {
        $tipos_gasto[] = [
            "id"        => $resultado["id_gasto"],
            "nombre"    => $resultado["nombre_gasto"]
        ];
    }

    // Buscamos los posibles pagadores
    $buscar_pagadores_sql = <<<SQL

        SELECT id_pagador,
               nombre_pagador
        FROM pagadores
        ORDER BY 2

SQL;

    $buscar_pagadores = mysqli_query($conexion, $buscar_pagadores_sql);
    if (!$buscar_pagadores) {
        echo "Error SQL buscar_pagadores: ";
        echo $buscar_pagadores_sql;
    }

    while ($resultado = mysqli_fetch_array($buscar_pagadores)) {
        $pagadores[] = [
            "id"        => $resultado["id_pagador"],
            "nombre"    => $resultado["nombre_pagador"]
        ];
    }

    $buscar_gasto_sql = <<<SQL

        SELECT p.id_pago,
               p.fecha_pago,
               p.importe_pago,
               p.descripcion_pago,
               p.id_pagador,
               pa.nombre_pagador,
               tp.nombre_gasto
        FROM pago p
        INNER JOIN pagadores pa
           ON p.id_pagador = pa.id_pagador
        INNER JOIN tipo_gasto tp
           ON p.id_gasto = tp.id_gasto
        WHERE p.id_pago = {$id_gasto}

SQL;

    $buscar_gasto = mysqli_query($conexion, $buscar_gasto_sql);
    if (!$buscar_gasto) {
        echo "Error SQL buscar_gasto" . $buscar_gasto_sql;
    } else {
        while ($resultado = mysqli_fetch_array($buscar_gasto)) {
            $gasto = [
                "id"            => $resultado["id_pago"],
                "fecha"         => $resultado["fecha_pago"],
                "importe"       => $resultado["importe_pago"],
                "pagador"       => $resultado["nombre_pagador"],
                "tipo_gasto"    => $resultado["nombre_gasto"],
                "descripcion"   => $resultado["descripcion_pago"]
            ];
        }
    }

    $fecha_gasto = (new DateTime($gasto["fecha"]))->format("Y-m-d");
    $fecha_hora_gasto = (new DateTime($gasto["fecha"]))->format("H:i");
    $importe_gasto = $gasto["importe"];
    $pagador_gasto = $gasto["pagador"];
    $tipo_gasto = $gasto["tipo_gasto"];   
    $id_gasto = $gasto["id"];
    $descripcion_gasto = $gasto["descripcion"];

?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
    <!-- Font Awesome -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  </head>
  <!-- Comienza el body -->
    <body>
    <!-- Cabecera: Panel de navegación -->
<?php 
    // Añadimos el menú
    require_once("navegacion.php");
?>
        <div class="container shadow">
            <div class="row central pb-5">
                <div class="col">
                    <h1>Editar gasto</h1>
                </div>
                <div class="w-100"></div>
                <div class="col">
                    <form method="post" action="gastos_editar.php">
                        <div class="form-row align-items-center">
                            <div class="col-auto">
                                <label for="fecha">Fecha</label>
                                <input type="date" name="fecha" class="form-control" value="<?php echo $fecha_gasto; ?>" required>
                            </div>
                            <div class="col-auto">
                                <label for="hora">Hora</label>
                                <input type="time" name="hora" class="form-control" value="<?php echo $fecha_hora_gasto; ?>" required>
                            </div>
                            <div class="col-auto">
                            <label for="pagador">Pagador</label>
                            <select name="pagador" class="custom-select">
                                <option value="" disabled required selected>Elige un pagador</option>
<?php 
foreach ($pagadores as $pagador) {
    if ($pagador["nombre"] == $pagador_gasto) {
        echo "
                                <option value=\"" . $pagador["id"] . "\" selected>" . $pagador["nombre"] . "</option>";
    } else {
        echo "
                                <option value=\"" . $pagador["id"] . "\">" . $pagador["nombre"] . "</option>";
    }
}
?>                        
                            </select>
                        </div>
                        <div class="col-auto">
                            <label for="tarea">Tipo de gasto</label>
                            <select name="tipo-gasto" class="custom-select">
                                <option value="" disabled required selected>Elige una categoría</option>
<?php 
foreach ($tipos_gasto as $gasto) {
    if ($gasto["nombre"] == $tipo_gasto) {
        echo "
                                <option value=\"" . $gasto["id"] . "\" selected>" . $gasto["nombre"] . "</option>";
    } else {
        echo "
                                <option value=\"" . $gasto["id"] . "\">" . $gasto["nombre"] . "</option>";
    }
}
?>                        
                            </select>
                        </div>
                        <div class="col-auto">
                            <label for="importe">Importe</label>
                            <input type="number" step="0.01" name="importe" class="form-control" required value="<?php echo $importe_gasto; ?>">
                        </div>
                        <div class="col-auto">
                            <label for="descripcion">Descripción</label>
                            <textarea name="descripcion" required class="form-control"><?php echo $descripcion_gasto; ?></textarea>
                        </div>
                            <div class="col-auto">
                                <input hidden type="text" name="id_gasto" value="<?php echo $id_gasto; ?>">
                                <input type="submit" name="enviar" value="Enviar" class="form-control">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php require_once("footer.php"); ?>
    </body>
</html>
<?php 
}
?>