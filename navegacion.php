<?php
session_start();
 
?>
	<!-- Cabecera: Panel de navegación -->
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#"><img src="images/icono.png" width="30" height="30" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item"><a href="principal.php" class="navbar-brand btn btn-light">Home</a></li>
                        <li class="nav-item"><a href="ficha.php?id=<?php echo $_SESSION["id_bebe"]; ?>" class="navbar-brand btn btn-light">Ficha de tu bebé</a></li>
                      <li class="nav-item"><a href="horario.php?id=<?php echo $_SESSION["id_bebe"]; ?>" class="navbar-brand btn btn-light">Horario</a></li>
                      <li class="nav-item"><a href="medicos.php?id=<?php echo $_SESSION["id_bebe"]; ?>" class="navbar-brand btn btn-light">Médicos</a></li>
                      <li class="nav-item"><a href="gastos.php?id=<?php echo $_SESSION["id_bebe"]; ?>" class="navbar-brand btn btn-light">Gastos</a></li>
                      <li class="nav-item"><a href="galeria.php?id=<?php echo $_SESSION["id_bebe"]; ?>" class="navbar-brand btn btn-light">Galería</a></li>
                      <li class="nav-item"><a href="logout.php?id=<?php echo $_SESSION["id_bebe"]; ?>" class="navbar-brand btn btn-light">Cerrar sesión</a></li>
                    </ul>
                </div>
            </nav>
