<?php
// Fichero que tomará los datos para la creación de la ficha
// del bebé, los comprobará y, si todo va bien, los añadirá
// a la base de datos

session_start();

require_once("conexion-bd.php");

if ($_POST["sexo"] == "a") {
	$sexo = "Niña";
} else {
	$sexo = "Niño";
}
//
// Subimos la imagen
// ================================================
$ruta_usuario = __DIR__ . "/users/" . $_SESSION["alias"];
// Si no existe el directorio donde almacena el usuario 
// las imágenes, lo creamos
if (!is_dir($ruta_usuario)) {
	mkdir($ruta_usuario);
}

// Definimos la ruta donde se guardará la imagen (directorio_usuario/nombre_imagen.jpg)
$ruta_imagen = "/users/" . $_SESSION["alias"] . "/" . round(microtime(true)) . "_" . $_FILES["imagen"]["name"];

// Definimos la ruta absoluta del servidor donde se guardará la imagen
// (/var/www/html/directorio_usuario/nombre_imagen.jpg)
$ruta_absoluta_imagen = __DIR__ . $ruta_imagen;
if (!move_uploaded_file($_FILES['imagen']['tmp_name'], $ruta_absoluta_imagen)) {
	echo "ERROR subiendo imagen: #" . $_FILES["imagen"]["error"];
} else {
	// echo "Imagen subida correctamente";
}

$fecha_nacimiento = $_POST["fecha"] . " " . $_POST["hora"];

$insertar_bebe_sql = <<<SQL

	INSERT INTO hijo (
		nombre,
		apellidos,
		sexo,
		fecha_nac,
		peso_nacer,
		altura_nacer,
		grupo_sanguineo,
		descripcion,
		imagen_hijo
	) VALUES (
		'{$_POST["nombre"]}',
		'{$_POST["apellidos"]}',
		'{$sexo}',
		'{$fecha_nacimiento}',
		'{$_POST["peso"]}',
		'{$_POST["altura"]}',
		'{$_POST["grupo-sangre"]}',
		'{$_POST["descripcion"]}',
		'{$ruta_imagen}'
	)

SQL;
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Babynfo</title>
    <meta charset="utf-8">
    <!-- para diseños responsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <meta name="description" content="Ficha de tu bebé" />
    <!-- css de Bootstrap -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" >
    <!-- mi CSS -->
    <link rel="stylesheet" href="css/custom.css" >
    <link rel="icon" href="images/icono.png" />
  </head>
  <!-- Comienza el body -->
  <body>
		<div class="container">
			<div class="row mt-3">
				<div class="col-lg-8 offset-lg-2">
<?php
// Introducimos el bebé en la base de datos
$insertar_bebe = mysqli_query($conexion, $insertar_bebe_sql);
if (!$insertar_bebe) {
	echo "Error SQL: insertar_bebe: ";
	echo $insertar_bebe_sql;
} else {
	$id_bebe = mysqli_insert_id($conexion);
	$id_usuario = $_SESSION["id"];

	// Añadimos al bebé y el usuario en la tabla que los relaciona
	$insertar_relacion_sql = <<<SQL

		INSERT INTO hijos_usuarios (
			id_usu,
			id_hijo
		) VALUES (
			$id_usuario,
			$id_bebe
		)

SQL;

	$insertar_relacion = mysqli_query($conexion, $insertar_relacion_sql);
	if (!$insertar_relacion) {
		echo "Error SQL: insertar_relacion: ";
		echo $insertar_relacion_sql;
	}

	echo "<p class=\"text-center\">Datos del bebé insertados correctamente. Ya puedes ir a la <a href=\"principal.php\">página principal</a></p>";
}
?>
				</div>
			</div>
		</div>
	</body>
</html>